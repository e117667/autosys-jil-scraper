const pageScraper = require("./pageScraper");
const loadFile = require("./loadFile");
const { Browser } = require("puppeteer-core");

/**
 *
 * @param {Browser} browserInstance
 */
async function scrapeAll(browserInstance) {
	const browser = await browserInstance;
	try {
		// Check Job list
		if (loadFile.jobList()[0] === "") {
			console.error("job.txt is empty!");
			await browser.close();
			return;
		}
		// Page Scraper do the job
		await pageScraper.scraper(browser);
	} catch (err) {
		console.log("Could not resolve the browser instance => ", err);
	}
	await browser.close();
}

module.exports = (browserInstance) => scrapeAll(browserInstance);

# Autosys JIL Scraper

## Purpose

This scraper is created for scraping data from Autosys JIL website

## Tech

- puppeteer-core
- node.js
- dotenv

## How to use

### Preparation

First fill the `job.txt` file with the box list, ex.:

    BOX-A
    BOX-B
    BOX-C

> For detail example please see `job-ex.txt`

After that copy `.env.example` & paste it with name `.env`

Inside `.env` configure / fill the data `LOGIN_*` ; `BROWSER_PATH` ; `URL` or `MAX_TIMEOUT`

After that run this command inside `shell` / `bash` / `cmd` / anything else for installing the all depedencies (make sure before run this command, the directory for this project has been mounted)

> `npm i`

or (if using yarn)

> `yarn`

### Run the Program

Run the program using this command

> `npm run start`

or

> `yarn start`

### Data Result

Result data from scraping Autosys website is save on `data.csv`.

> **IMPORTANT NOTE** :
>
> - If need to re-run the program with new job / box list or something else please save the `data.csv` with other name or anything because the program will replace `data.csv` with new data
> - Make sure your internet connection is fast enough or the scraper will not doing it's job properly

Made with 💖Fernando Gunawan

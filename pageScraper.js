const { Browser } = require("puppeteer-core");
const loadFile = require("./loadFile");
const dotenv = require("dotenv");
dotenv.config();

const scraperObject = {
	url: process.env.URL,
	/**
	 *
	 * @param {Browser} browser
	 */
	async scraper(browser) {
		const page = await browser.newPage();
		await page.setRequestInterception(true);
		page.setDefaultNavigationTimeout(process.env.MAX_TIMEOUT);
		page.setDefaultTimeout(process.env.MAX_TIMEOUT);
		page.setCacheEnabled(true);
		page.on("request", (req) => {
			if (req.resourceType() == "font") {
				req.abort();
			} else {
				req.continue();
			}
		});
		await page.goto(this.url, {
			waitUntil: "networkidle0",
		});
		// Login to AutosysJIL
		console.log("Login to AutosysJIL");
		try {
			await page.waitForSelector("#x-auto-8-input");
			await page.type("#x-auto-8-input", process.env.LOGIN_USERNAME);
			await page.type("#x-auto-9-input", process.env.LOGIN_PASSWORD);
			await page.click(".x-btn-text", { delay: 500 });
			await page.waitForNetworkIdle({ idleTime: 1500 });
			await page.waitForTimeout(2000);
			console.log("Login Finish");
		} catch (error) {
			console.error(
				"Login failed, please check username & password or check this error " +
					error
			);
			return;
		}
		// Select Quick Search tab
		console.log("Navigate to Quick Search tab");
		await page.waitForSelector("#x-auto-76__x-auto-286");
		await page.click("#x-auto-76__x-auto-286", { delay: 500 });
		await page.waitForNetworkIdle({ idleTime: 1500 });
		// Locate the iframe because for searching job is using iframe
		const frameEl = await page.waitForSelector("#x-auto-286 iframe");
		const frame = await frameEl.contentFrame();
		// Redirect to iframe URL
		await page.goto(frame.url());
		await page.waitForNetworkIdle({ idleTime: 1500 });
		const boxList = loadFile.jobList();
		const allData = [];
		await page.waitForSelector("#jobNameInput");
		// Scraping the data
		for (const data of boxList) {
			console.log(`Scraping job from ${data} BOX`);
			// Search job / box
			await page.type("#jobNameInput", data);
			await page.keyboard.press("Enter");
			await page.waitForNetworkIdle({ idleTime: 1500 });
			let rows = await page.$$(
				"#boxJobResultTable > table.af_treeTable_content > tbody > tr"
			);
			// Open every branch
			for (let x = 0; x < rows.length - 1; x++) {
				try {
					await page.waitForSelector(`#boxJobResultTable\\:${x}\\:hgi`, {
						timeout: 1500,
					});
					await page.click(`#boxJobResultTable\\:${x}\\:hgi`);
					await page.waitForNetworkIdle({ idleTime: 1500 });
					rows = await page.$$(
						"#boxJobResultTable > table.af_treeTable_content > tbody > tr"
					);
				} catch (error) {}
			}
			console.log(`Total Box + Job from ${data}: ${rows.length - 1}`);
			// The magic works here ✨
			for (let i = 1; i < rows.length; i++) {
				const link = await rows[i].$eval("a.OraLink", (el) => el.href);
				const jobName = await rows[i].$eval(
					"a.OraLink span",
					(el) => el.textContent
				);
				console.log(`Scraping data ${jobName} .....`);
				const dataPage = await browser.newPage();
				await dataPage.goto(link);
				await dataPage.waitForNetworkIdle();
				const dataRows = await dataPage.$$(
					"#treeTableId > table.af_treeTable_content > tbody > tr"
				);
				let tempData = new Array(15).fill(" ");
				for (let z = 1; z < dataRows.length; z++) {
					const row = dataRows[z];
					const index = await row.$eval("td:nth-child(1)", (el) =>
						el.textContent.replace("\n\n\n\n\n\n\n\n", "")
					);
					const val = await row.$eval("td:nth-child(2)", (el) =>
						el.textContent.replace("\n\n\n\n\n\n\n\n", "")
					);
					if (val === "") continue;
					tempData = insertData(tempData, index, val);
				}
				allData.push(tempData);
				console.log(tempData);
				loadFile.saveData(allData);
				console.log(`Scraping data ${jobName} DONE`);
				await dataPage.close();
			}
			await page.waitForTimeout(1000);
			await page.click("#jobNameInput", { clickCount: 3 });
			await page.type("#jobNameInput", "");
		}
		console.log("Scarping Box & Job finish");
	},
};

const insertData = (data, index, val) => {
	switch (index) {
		case "Name":
			data[0] = val;
			break;
		case "Description":
			data[1] = val;
			break;
		case "Send to machine":
			data[2] = val;
			break;
		case "Condition":
			data[3] = val;
			break;
		case "Owner":
			data[4] = val;
			break;
		case "Application":
			data[5] = val;
			break;
		case "Box":
			data[6] = val;
			data[13] = "JOB";
			break;
		case "Command":
			data[10] = val;
			break;
		case "Run calendar":
			data[9] = val;
			break;
		case "Times of day":
			data[8] = `"${val}"`;
			break;
		case "Default Email address":
			data[12] = val;
			break;
		case "Permission":
			data[14] = val;
			break;
		case "Message":
			data[11] = val;
			break;
		case "Minutes after each hour":
			data[7] = `"${val}"`;
			break;
		case "Status Time":
			data[15] = val;
		default:
			break;
	}
	return data;
};

module.exports = scraperObject;

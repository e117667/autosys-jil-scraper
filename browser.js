const puppeteer = require("puppeteer-core");
const dotenv = require("dotenv");
dotenv.config();

/**
 *
 * @return {puppeteer.Browser}
 */
async function startBrowser() {
	let browser;
	try {
		console.log("Opening the browser......");
		browser = await puppeteer.launch({
			headless: true,
			args: ["--disable-setuid-sandbox"],
			ignoreHTTPSErrors: true,
			executablePath: process.env.BROWSER_PATH,
		});
	} catch (err) {
		console.log("Could not create a browser instance => : ", err);
	}
	return browser;
}

module.exports = {
	startBrowser,
};

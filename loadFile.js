const fs = require("fs");
const { fileURLToPath } = require("url");

/**
 *
 * @return {string|string[]}
 */
const jobList = () => {
	try {
		let data = fs.readFileSync("./job.txt", "utf8").split("\n");
		return data.map((d) => {
			return d.replace("\r", "");
		});
	} catch (err) {
		return err;
	}
};

/**
 *
 * @param {string[]} data
 * @return {boolean} result
 */
const saveData = (data) => {
	try {
		let res = data.map((d) => {
			return d.join(";");
		});
		fs.writeFileSync("./data.csv", res.join("\n"), { encoding: "utf-8" });
		return true;
	} catch (err) {
		console.log(err);
		return false;
	}
};

module.exports = { jobList, saveData };
